class TasksController < ApplicationController
	#require 'resque_scheduler'
	require 'resque/server'
	def index
		@task = Task.all
	end

	def show
		@task = Task.find(params[:id])
	end

	def new
		@task = Task.new
	end

	def create
		@task = Task.new(param_task)
		if @task.save
			#set delayed job
			#Resque.enqueue_at(@task.start_time,Test,@task.id)
			#set Dynamic scheduler
			# name = 'mailsmail'
			# config = {}
			# config[:class] = 'Album'
			# config[:args] = 'PO'
			# config[:every] = ['1h', {first_in: 2.minutes}]
			# config[:persist] = true
			#Resque.set_schedule(name, config)
			
			#convert_cron_time(@task)
			# Resque.set_schedule(@task.name, {
			#   :cron => "/1 * * * *",	
			#   :class => 'CreateQueue',
			#   :queue => @task.server_name,
			#   :message => 'notification message'      
			# })
			

			redirect_to tasks_path
		else
			render :new
		end 
	end

	def edit
		@task = Task.find(params[:id])
	end
	
	def update
		@task = Task.find(params[:id])
		if @task.update_attributes(param_task)
			Resque.set_schedule(@task.name, {
			  :cron => "/1 * * * *",	
			  :class => 'CreateQueue',
			  :queue => @task.server_name,
			  :message => 'notification message'      
			})
	  	redirect_to tasks_path
	  else
	  	render :edit
	  end
	end	

	def destroy
		@task = Task.find(params[:id])
		Resque.remove_schedule(@task.server_name)
		@task.destroy
		redirect_to tasks_path
	end

	# def convert_cron_time task
	# 	yr 	 =  @task.start_date.strftime("%Y").to_i
	# 	mnth =  @task.start_date.strftime("%m").to_i
	# 	date = @task.start_date.strftime("%d").to_i
	# 	hr =   @task.start_time.strftime("%I").to_i
	# 	min = @task.start_time.strftime("%M").to_i
	# 	hr = hr + 12 if @task.start_time.strftime("%p") == "PM"
	#   cron = 0 + " " + min + " " + date + " " + mnth + " " + "?" + " " + yr
	# end
	private
		def param_task
			params.require(:task).permit(:name, :start_date, :start_time, :end_date, :end_time, :server_name, :executable_path)
		end
end
