class Task < ActiveRecord::Base
	validates :name, presence: true, length: {maximum: 255}, uniqueness: true
	validates :server_name, presence: true, uniqueness: true
	validates :executable_path, presence: true
  
end
