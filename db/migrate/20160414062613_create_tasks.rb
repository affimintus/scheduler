class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
    	t.string :name
    	t.datetime :start_date 
    	t.time :start_time
    	t.datetime :end_date
    	t.time :end_time 
    	t.string :week_days 
    	t.string :server_name
    	t.string :executable_path

      t.timestamps null: false
    end
  end
end
